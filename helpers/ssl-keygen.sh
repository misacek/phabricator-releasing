#!/bin/bash -x

hostname=$(hostname --fqdn)
directory='/opt/phacility/certs'
key="$directory/${hostname}.key"
crt="$directory/${hostname}.crt"
csr="$directory/${hostname}.csr"

## # server key
## openssl genpkey  -algorithm RSA -pkeyopt rsa_keygen_bits:4096 -out $key
## 
## # generate csr
## openssl req -new -key $key -out $csr -subj /CN=$hostname/OU=Le unit/
## 
## # generate certificate
## openssl x509 -req -days 3650 -in $csr -signkey $key -out $crt
## 
## #inspect certificate subject
## echo openssl x509 -in $crt -noout -subject

openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout $key -out $crt -subj "/CN=$hostname/"

# certificate check
echo "your certificate: $crt"
openssl x509 -in $crt -subject -dates -noout


