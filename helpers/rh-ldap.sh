#!/bin/bash -x
trap exit ERR

yum -y install openldap openldap-clients

cat > /etc/openldap/ldap.conf << AA
URI ldaps://ldap.corp.redhat.com
SASL_SECPROPS none
TLS_CACERT /etc/openldap/rhca.crt
BASE ou=Users,dc=redhat,dc=com
AA

wget -q -O /etc/openldap/rhca.crt \
    https://password.corp.redhat.com/cacert.crt
chmod 644 /etc/openldap/rhca.crt

# to test use this
#ldapsearch \
#    -H ldaps://ldap.corp.redhat.com:636 \
#    -b 'ou=Users,dc=redhat,dc=com' \
#    -x uid=mnovacek \
#    -W

/opt/phacility/phabricator/bin/config set auth.require-approval false
mysql -u root phabricator_auth < ./helpers/ldap-mysql
echo 'Administator is admin with password admin.'

if [ -x /usr/bin/systemctl ];
then
    systemctl restart httpd
else
    service httpd restart
fi
