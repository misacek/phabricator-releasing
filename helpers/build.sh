#!/bin/bash -x
trap exit ERR

function prepare_build(){
    # Prepares everythin in the SOURCES directory so you can rpmbuild.
    #
    # Download tarball of origin/master for the project specified by the first
    # parameter in the form of 'user/project'. The downloaded tarball will be
    # named <project>-<commit>.tar.gz and saved in ~/rpmbuild/SOURCES directory
    # or if it does not exist to the sources/ directory created in current
    # directory.

    trap exit ERR
    # split input string to USER and PROJECT
    IFS='/' read -ra ARR <<< "$1"
    USER=${ARR[0]}
    PROJECT=${ARR[1]}

    # check that the first parameter has been given and that it is correct
    if [[ -z "$1" || -z "$USER" || -z "$PROJECT" ]];
    then
        echo 'You must specify github user/project as first parameter.'
        exit 1
    fi

    GITURL="https://github.com/$1"

    # check that the url is valid
    wget -q -O /dev/null $GITURL

    # find out commit id of HEAD of branch master
    long_commit_id=$(git ls-remote $GITURL master | cut -f 1)
    commit_id=${long_commit_id:0:7} #return first 7 chars of commit id

    wget -P "$RPMBUILD/SOURCES" -q $GITURL/archive/${long_commit_id}/$PROJECT-${long_commit_id}.tar.gz
    ln -fs $PROJECT-${long_commit_id}.tar.gz "$RPMBUILD"/SOURCES/$PROJECT.tar.gz

    echo ${commit_id} > $RPMBUILD/SOURCES/"$PROJECT-revision"

    # copy files that we add to those projects to $RPMBUILD/SOURCES
    cp phabricator.init phabricator.service phabricator-httpd*.conf $RPMBUILD/SOURCES
}

function create_rpmbuild_structure(){
    trap exit ERR

    # remove all files inside the structure
    [ -d "$RPMBUILD" ] && find $RPMBUILD -type f -exec rm -f {} \;

    # create the structure
    for dir in BUILD BUILDROOT RPMS SOURCES SPECS SRPMS:
    do
        mkdir -p $RPMBUILD/$dir
    done
}

function copy_created_rpms_to_repo(){
    trap exit ERR

    for DIST in el6 el7;
    do
        REPOSITORY="$REPOSITORY_USER@$REPOSITORY_HOST"
        RPMS=''
        for rpm in $(find . -type f -name "*$DIST*noarch.rpm"); do RPMS+="$rpm "; done
        SRPMS=''
        for srpm in $(find . -type f -name "*$DIST*.src.rpm"); do SRPMS+="$srpm "; done

        if [ -z "$RPMS" ];
        then
            echo "No rpm files found, exiting"
            continue
        fi

        _REPOSITORY_DIR="$REPOSITORY_DIR/$DIST"

        # verify that directory exist or create it
        ssh -q $REPOSITORY "[ -d \"$_REPOSITORY_DIR\" ] || mkdir -p \"$_REPOSITORY_DIR\""
        ssh -q $REPOSITORY "[ -d \"$_REPOSITORY_DIR/srpms\" ] || mkdir -p \"$_REPOSITORY_DIR/srpms\""
        # verify that we do have createrepo or try to install it
        ssh -q $REPOSITORY 'which createrepo || yum -y install createrepo'
        # copy created rpms to repository
        rsync -avz $RPMS $REPOSITORY:$_REPOSITORY_DIR
        rsync -avz $SRPMS $REPOSITORY:$_REPOSITORY_DIR/srpms
        # create repository metadata
        ssh -q $REPOSITORY "createrepo --database --deltas $_REPOSITORY_DIR" &
        ssh -q $REPOSITORY "createrepo --database --deltas $_REPOSITORY_DIR/srpms" &
        # create repofile
        ssh -q $REPOSITORY "
        cat << AA > $_REPOSITORY_DIR/../${DIST}.repo
[phabricator-${DIST}]
name=phabricator-${DIST}
enabled=1
gpgcheck=0
baseurl=http://${REPOSITORY_HOST}${_REPOSITORY_DIR}

[phabricator-${DIST}-srpms]
name=phabricator-${DIST}-srpms
enabled=1
gpgcheck=0
baseurl=http://${REPOSITORY_HOST}${_REPOSITORY_DIR}/srpms
AA
" &
    done
    echo 'Waiting for all processes to finish...'
    wait
}

function prepare(){
    create_rpmbuild_structure

    for project in $PROJECTS;
    do
        prepare_build $project
    done
}


export RPMBUILD='rpmbuild'
export PROJECTS=("
    phacility/libphutil
    phacility/phabricator
    phacility/arcanist
    psigen/libphremoteuser
")

export REPOSITORY_HOST=${REPOSITORY_HOST:-'zapp.cluster-qe.lab.eng.brq.redhat.com'}
export REPOSITORY_USER=${REPOSITORY_USER:-'qa-cron'}
export REPOSITORY_DIR=${REPOSITORY_DIR:-'/dist/repos/phabricator'}

case $1 in
    prepare)
        prepare
    ;;
    build)
        prepare

        # build rpms and srpms
        rpmbuild --define "_topdir $(pwd)/$RPMBUILD" -ba phabricator.spec
    ;;
    srpm)
        prepare

        # build rpms and srpms
        rpmbuild --define "_topdir $(pwd)/$RPMBUILD" -bs phabricator.spec
    ;;
    repo-release)
        copy_created_rpms_to_repo
    ;;
    *)
        build
        copy_created_rpms_to_repo
    ;;
esac

