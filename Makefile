export REPOSITORY_HOST=zapp.cluster-qe.lab.eng.brq.redhat.com
export REPOSITORY_USER=mnovacek
export REPOSITORY_DIR=/dist/repos/phabricator#without the distro final dir

help:
	@echo "Possible targets:"
	@echo "	prepare:  prepares SOURCES/ so you can build with rpmbuild"
	@echo "	build (aliases: rpm, rpms): creates rpms and srpms"
	@echo "	mock-build (alias: mock): builds (s)rpms for rhel6 and rhel7 with mock"
	@echo "	srpm: creates srpm only"
	@echo "	repo-release (aliases: release): copy rpms to yum repository"

prepare:
	./helpers/build.sh prepare_build

rpm: rpms
build: rpms
rpms:
	./helpers/build.sh build

srpm:
	./helpers/build.sh srpm

release: repo-release
repo-release:
	./helpers/build.sh repo-release

all: rpms repo-release

# builds for el6 and el7
mock-build: mock
mock: srpm
	rm -rf builds; \
	for version in 6 7; do \
	/usr/bin/mock \
		-r ./helpers/epel-$${version}-noarch.cfg \
		--cleanup-after \
		--resultdir ./builds/"%(dist)s"/"%(target_arch)s"/ \
		./rpmbuild/SRPMS/*.src.rpm & \
	done; \
	wait

