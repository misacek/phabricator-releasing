
# naming is <name>-0.<weeknumber>-YYYYMMDDHHMMSSgit<commit id>.<dist tag>

%define version 0.%(date +%U)
%define build_timestamp %(date +"%Y%m%d%H%M")

Name:           phabricator
Version:        %{version}
Summary:        Collection of web applications to help build software
Packager:       Michal Nováček <mnovacek@redhat.com>

URL:            http://www.phabricator.org
Source0:        https://github.com/phacility/libphutil/archive/master/libphutil.tar.gz
Source10:       libphutil-revision
Source1:        https://github.com/phacility/libphutil/archive/master/arcanist.tar.gz
Source11:       arcanist-revision
Source2:        https://github.com/phacility/libphutil/archive/master/phabricator.tar.gz
Source21:       phabricator-revision
Source3:        https://github.com/psigen/libphremoteuser/archive/master/libphremoteuser.tar.gz
Source31:       libphremoteuser-revision
Source4:        phabricator.init
Source5:        phabricator.service
Source6:        phabricator-httpd.conf
Source7:        phabricator-httpd-ssl.conf

Group:          Development/System
License:        Apache 2.0
BuildArch:      noarch
Release:        %{build_timestamp}git%(cat %SOURCE21)%{?dist}

# common requirements for all versions
BuildRequires:  git
Requires(pre):  shadow-utils
Requires:       git httpd mod_ssl
Requires:       php php-cli php-devel php-gd php-ldap php-mbstring php-mysql
Requires:       php-pecl-json php-pecl-memcache php-process python-pygments
Requires:       phabricator-arcanist = %{version}
Requires:       phabricator-libphutil = %{version}
Requires:       phabricator-libphremoteuser = %{version}
Requires(post): openssl
AutoReq:        no

# requirements separated by rhel version
%if 0%{?rhel} < 7
Requires: mysql-server
Requires(post): chkconfig
Requires(preun): chkconfig initscripts
%else
BuildRequires: systemd
Requires: mariadb-server
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
%endif


%description
Phabricator is an open source collection of web applications which help
software companies build better software.

%package arcanist
Summary:        command-line interface to Phabricator
Release:        %{build_timestamp}git%(cat %SOURCE11)%{?dist}
Requires:       php-cli
Requires:       phabricator-libphutil = %{version}
AutoReq:        no

%description arcanist
Arcanists provides command-line access to many Phabricator tools (like
Differential, Files, and Paste), integrates with static analysis ("lint") and
unit tests, and manages common workflows like getting changes into Differential
for review.

%define commit %(cat %SOURCE12)
%package libphutil
Summary:        a collection of utility classes and functions for PHP
Release:        %{build_timestamp}git%(cat %SOURCE10)%{?dist}
AutoReq:        no

%description libphutil
libphutil is a collection of utility classes and functions for PHP. Some
features of the library include:
- libhutil library system
- futures
- filesystem
- xsprintf
- AAST/PHPAST
- Remarkup
- Daemons
- Utilities

%package libphremoteuser
Summary:        Phabricator extension to authenticate with apache REMOTE_USER variable.
Release:        %{build_timestamp}git%(cat %SOURCE31)%{?dist}
Requires:       php-cli
Requires:       phabricator-libphutil = %{version}
AutoReq:        no

%description libphremoteuser
This extension to Phabricator performs basic authentication via a web server's
REMOTE_USER variable. It should be able to work with a variety of major servers
such as Apache and Nginx. This can be used to use single sign on with kerberos.

%prep
for source in %SOURCE0 %SOURCE1 %SOURCE2 %SOURCE3;
do
    mkdir -p $(basename $source .tar.gz) && \
    tar -C $(basename $source .tar.gz) -xf $source --strip-components=1
done

%build
echo Nothing to build.

%install
DEST=${RPM_BUILD_ROOT}/opt/phacility
mkdir -p ${DEST}
mkdir -p ${DEST}/certs

mkdir -p ${RPM_BUILD_ROOT}%{_var}/lib/phabricator
mkdir ${RPM_BUILD_ROOT}%{_var}/lib/phabricator/files
mkdir ${RPM_BUILD_ROOT}%{_var}/lib/phabricator/repo


%if 0%{?rhel} < 7
mkdir -p ${RPM_BUILD_ROOT}%{_initddir}
install -m 644 %{SOURCE4} ${RPM_BUILD_ROOT}%{_initddir}/phabricator
%else
mkdir -p ${RPM_BUILD_ROOT}%{_unitdir}
install -m 644 %{SOURCE5} ${RPM_BUILD_ROOT}%{_unitdir}/phabricator.service
%endif

mkdir -p ${RPM_BUILD_ROOT}%{_var}/log/phabricator
mkdir -p ${RPM_BUILD_ROOT}/%{_var}/run/phabricator

mkdir -p ${RPM_BUILD_ROOT}/etc/httpd/conf.d
install -m 644 %{SOURCE6} ${RPM_BUILD_ROOT}/etc/httpd/conf.d/phabricator.conf
install -m 644 %{SOURCE7} ${RPM_BUILD_ROOT}/etc/httpd/conf.d/ssl.phabricator.conf

for dir in phabricator libphutil arcanist libphremoteuser;
do
    mv -v ${RPM_BUILD_DIR}/$dir ${RPM_BUILD_ROOT}/opt/phacility
done

%clean
rm -rf $RPM_BUILD_ROOT

%pre
getent group phabricator >/dev/null || groupadd -r phabricator
getent passwd phabricator >/dev/null || \
    useradd -r -g phabricator -d %{_var}/lib/phabricator -s /sbin/nologin \
    -c "Daemon user for Phabricator" phabricator

%post
#!/bin/sh
function tweak_phabricator(){
    LOGFILE=$(mktemp --tmpdir=/tmp phabricator-tweak_phabricator-$(date +%Y%M%d-%H%m)-XXXXXXXX)
    2>>$LOGFILE

    JSON=/opt/phacility/phabricator/conf/local/local.json
    [ -e ${JSON} ] && cp ${JSON} ${JSON}-$(date +%Y%M%d-%H%m)

    function add_config_value(){
        CFG=/opt/phacility/phabricator/bin/config
        VARIABLE="$1"
        VALUE="$2"

        if grep -q $VARIABLE $JSON;
        then
            echo "$VARIABLE already exists in $JSON" >> $LOGFILE
        else
            $CFG set $VARIABLE "$VALUE"
        fi
    }

    echo
    echo "Creating default values in $JSON when they do not exist already..."
    add_config_value repository.default-local-path %{_var}/lib/phabricator/repo
    add_config_value storage.local-disk.path %{_var}/lib/phabricator/files
    add_config_value mysql.host localhost
    add_config_value mysql.user phabricator
    add_config_value mysql.pass phabricator
    add_config_value pygments.enabled true
    add_config_value phabricator.base-uri http://$(hostname -f)/
    add_config_value metamta.default-address phabricator@$(hostname -f)
    add_config_value metamta.domain $(hostname -f)
    add_config_value phd.user phabricator
    add_config_value phd.log-directory %{_var}/log/phabricator
    add_config_value phd.pid-directory %{_var}/run/phabricator
    add_config_value diffusion.allow-http-auth true
    add_config_value phabricator.csrf-key \
    $(dd if=/dev/urandom bs=128 count=1 2>/dev/null |  base64 | grep -E -o '[a-zA-Z0-9]' | head -30 | tr -d '\n')
    add_config_value phabricator.mail-key \
    $(dd if=/dev/urandom bs=128 count=1 2>/dev/null |  base64 | grep -E -o '[a-zA-Z0-9]' | head -30 | tr -d '\n')
    add_config_value load-libraries '["/opt/phacility/libphremoteuser/src"]'

    # base url is httpS
    /opt/phacility/phabricator/bin/config set phabricator.base-uri "https://$(hostname --fqdn)"

    /opt/phacility/phabricator/bin/storage upgrade --force >/dev/null

    if [ -s $LOGFILE ]; #exists and has size bigger than 0
    then
        echo
        echo "There might have been problem with tweaking phabricator."
        echo "Check log file $LOGFILE"
        echo
    fi
}

# Httpd needs access to the repo folder
function add_apache_to_group_phabricator() {
    if ! groupmems -g phabricator -l | grep -q apache; then
      groupmems -g phabricator -a apache
    fi
}

# Enable and start mariadb or mysql
function enable_start_mysql(){
    %if 0%{?rhel} < 7
        /sbin/chkconfig --add mysqld
        /sbin/service mysqld start
    %else
        systemctl enable mariadb
        systemctl start mariadb
    %endif
}

#enable phabricator start on reboot
function enable_start_phabricator(){
    %if 0%{?rhel} < 7
        /sbin/chkconfig --add phabricator
        /sbin/chkconfig phabricator on
    %else
        %systemd_post phabricator.service
    %endif
}

# create default phabricator account in mysql and grant it access to
# phabricator db
function tweak_mysql() {
    LOGFILE=$(mktemp --tmpdir=/tmp phabricator-tweak_mysql-$(date +%Y%M%d-%H%m)-XXXXXXXXX)

    if mysql -u root -e "show databases" &>/dev/null;
    then
        mysql -u root -e "create user 'phabricator'@'localhost';" 2>>$LOGFILE
        mysql -u root -e "grant all privileges on *.* to 'phabricator'@'localhost';" 2>>$LOGFILE
        mysql -u root -e "set password for 'phabricator'@'localhost' = password('phabricator');" 2>>$LOGFILE && \
        echo
        echo "BEWARE: user phabricator have default KNOWN password set."

        if [ -s $LOGFILE ];
        then
            echo
            echo "There might have been problem with the changes made to mysql "
            echo "database. Check $LOGFILE"
        fi
    else
        echo
        echo "You have password set for mysql user 'root@localhost'."
        echo "You need to create user phabricator and database phabricator on "
        echo "your own."
    fi
}

# generate self-signed certificates for our hostname, save them to
# /opt/phacility/certs/<hostname>.{key,crt} and update the configuration
function certificates_generation(){
    set -x
    HOSTNAME=$(hostname --fqdn)
    if [ -f /opt/phacility/certs/${HOSTNAME}.key ] || [ -f /opt/phacility/certs/${HOSTNAME}.crt ];
    then
        echo
        echo "Not generating certificates as at least one of key/crt exists."
    else
        LOGFILE=$(mktemp --tmpdir=/tmp phabricator-certificates-generation-$(date +%Y%M%d-%H%m)-XXXXXXXXXX)
        2>>$LOGFILE

        echo "Generating self-signed certificates for $HOSTNAME..."

        openssl req \
            -new -x509 -nodes -days 3650 -newkey rsa:4096 \
            -keyout /opt/phacility/certs/${HOSTNAME}.key \
            -out /opt/phacility/certs/${HOSTNAME}.crt \
            -subj "/CN=${HOSTNAME}" \
            1>/dev/null

        if [ -s $LOGFILE ];
        then
                cat << AA >> $LOGFILE
# certificate can be inspected show with this command
# openssl x509 -in /opt/phacility/certs/${HOSTNAME}.crt -text -noout
AA

            echo
            echo -n "There might have been error generating certificates. "
            echo "Check log file: $LOGFILE"
        fi
    fi

    # change certificate names in phabricator-ssl.conf
    sed -i -e "s/^.*SSLCertificateKeyFile.*/    SSLCertificateKeyFile \/opt\/phacility\/certs\/${HOSTNAME}.key/" \
        /etc/httpd/conf.d/ssl.phabricator.conf
    sed -i -e "s/^.*SSLCertificateFile.*/    SSLCertificateFile \/opt\/phacility\/certs\/${HOSTNAME}.crt/" \
        /etc/httpd/conf.d/ssl.phabricator.conf
}

# configure httpd:
#   * use fqdn as servername
#   * replace allow all in <Directory> based on rhel (rhel6: apache 2.2, rhel7: apache: 2.4)
function fqdn_in_apache_config() {
    for file in ssl.phabricator.conf phabricator.conf;
    do
        sed -i -e "s/this-is-your-servername/$(hostname --fqdn)/" \
            /etc/httpd/conf.d/$file
    done

    ALLOW_ALL='Allow from all'
    %if 0%{?rhel} > 6
        ALLOW_ALL='Require all granted'
    %endif
    for file in ssl.phabricator.conf phabricator.conf;
    do
        sed -i -e "s/..allow-from-all-or-granted/$ALLOW_ALL/" \
            /etc/httpd/conf.d/$file
    done
}

function selinux() {
    if which selinuxenabled &>/dev/null;
    then
        selinuxenabled && setsebool -P httpd_can_connect_ldap 1
    fi
}


# -------------

enable_start_mysql
tweak_mysql

tweak_phabricator
enable_start_phabricator

fqdn_in_apache_config
add_apache_to_group_phabricator
selinux

certificates_generation

%preun
%if 0%{?rhel} < 7

    if [ "$1" == 0 ];
    # ^^ checks that this is the actual deinstallation of the package, as opposed
    # to just removing the old package on upgrade.
    then
        /sbin/service phabricator stop >/dev/null 2>&1
        /sbin/chkconfig --del phabricator
    fi

%else

    %systemd_preun phabricator.service

%endif

%postun
if [ "$1" == 0 ];
# ^^ checks that this is the actual deinstallation of the package, as opposed
# to just removing the old package on upgrade.
then
    if which mysql &>/dev/null;
    then
        DATABASES=$(mysql -u root -e 'show databases;' | grep phabricator)
        SQLFILE=$(mktemp --tmpdir=/tmp phabricator-drop-database-$(date +%Y%M%d-%H%m)-XXXXXXXXXX)
        for d in $DATABASES;
        do
            echo drop database $d\; >> $SQLFILE
        done
        echo "drop user 'phabricator'@'localhost';" >> $SQLFILE
        echo
        echo "MySQL data not removed."
        echo "You can remove them manually by running: mysql -u root < $SQLFILE"
        echo
    fi
fi

%if 0%{?rhel} >= 7
%systemd_postun_with_restart phabricator.service
%endif

%files
%defattr(-,root,root,-)
/opt/phacility/certs
/opt/phacility/phabricator
/etc/httpd/conf.d/phabricator.conf
/etc/httpd/conf.d/ssl.phabricator.conf

%if 0%{?rhel} < 7
%attr(0755,-,-) %{_initddir}/phabricator
%else
%attr(0755,-,-) %{_unitdir}/phabricator.service
%endif

%dir %attr(0750, phabricator, phabricator)%{_var}/lib/phabricator
%dir %attr(2750, phabricator, phabricator) %{_var}/lib/phabricator/repo
%dir %attr(0700, apache, apache) %{_var}/lib/phabricator/files
%dir %attr(0750, phabricator, phabricator)%{_var}/log/phabricator
%dir %attr(0750, phabricator, phabricator)%{_var}/run/phabricator

%files arcanist
/opt/phacility/arcanist

%files libphutil
/opt/phacility/libphutil

%files libphremoteuser
/opt/phacility/libphremoteuser

%changelog
